# -*- coding: utf-8 -*-
# -*- mode: org -*-

#+STARTUP: BEAMER

#+TITLE:    Grundlagen der Elektrotechnik 2 (GE2)
#+SUBTITLE: 3 Zeitveränderliche Ströme und Spannungen an Kondensatoren und Spulen
#+AUTHOR:   \href{mailto:johanna.may@th-koeln.de}{Prof. Dr. Johanna Friederike May}
#+INSTITUTION: Institut für Elektrische Energietechnik (IET) und Cologne Institute for Renewable Energy (CIRE)
#+EMAIL: johanna.may@th-koeln.de
#+DATE:     \today
#+DESCRIPTION:  
#+KEYWORDS: 
#+LANGUAGE: de

#+OPTIONS:  H:2 texht:t toc:nil title:nil

# #+OPTIONS:  TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
# #+INFOJS_OPT: view:nil toc:t ltoc:t mouse:underline buttons:0  path:https://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:
#+LINK_HOME:

#+INCLUDE: "../slidehead.org"
# #+INCLUDE: "../GE2skript/printhead2.org"

# # Glossar bisher funktioniert das nach der Anleitung [[https://orgmode.org/worg/exporters/beamer/beamer-dual-format.html][_hier_]] nicht
# #+INCLUDE: "../GE2skript/glossary.org"

# #+name: set-slide-flag
# #+begin_src emacs-lisp :exports results :results value latex
# (setq hjh-exporting-slides 't)
# ""
# #+end_src

# #+call: makegloss :exports (if hjh-exporting-slides "results" "none") :results value latex
# #+results: makegloss

\begin{frame}
\maketitle
\begin{tikzpicture}[overlay, remember picture]
\node[above right=7.5cm and 9.3cm of current page.south west] {\includegraphics[width=2cm]{/home/franzi/Bilder/TH/thk.pdf}};
\end{tikzpicture}
\end{frame}

\frame[t]{\tableofcontents}

#+name: fs
#+begin_src python :results value :session :exports none
fontsize=18
return fontsize
#+end_src

#+include: "./03GE2LCZeitFu.org" :minlevel 1

* Verwendete Literatur
#+BEGIN_EXPORT latex
\printbibliography
#+END_EXPORT

#+BEGIN_EXPORT latex
\begin{comment}
#+END_EXPORT
#+BIBLIOGRAPHY: alphabetic
#+BEGIN_EXPORT latex
\end{comment}
#+END_EXPORT



# # literatur wie Glossar 
