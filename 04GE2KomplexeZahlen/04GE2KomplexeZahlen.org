#+STARTUP: BEAMER
* Lernziele und Struktur dieser Einheit
** Lernziele dieser Vorlesung :B_frame:
*** Sie können *Effektivwertzeiger* anstelle von *cosinusförmigen* Wechselsignalen verwenden:
    #+BEAMER:\pause
    - Betrag und Phase ermitteln
    #+BEAMER:\pause
    - Wirk- und Blindanteil bestimmen
    #+BEAMER:\pause
*** Sie können mit *komplexen Zahlen* rechnen: 
    - addieren, subtrahieren, invertieren, dividieren, multiplizieren
    #+BEAMER:\pause
    - integrieren, differenzieren
* Vorstellung des heutigen Anwendungsbeispiels

** Welcher Strom fließt durch die Mehrfachsteckdose?

    #+NAME: fig-mehrfachsteckdose
    #+ATTR_LATEX: :width .5\textwidth :height .5\textheight :options angle=0,keepaspectratio :float nil
    #+CAPTION: Mehrfachsteckdose mit mehreren Verbrauchern: Annahme alle nutzen sinusförmigen Wechselstrom \normalsize
    [[../GE2img/mehrfachsteckdose.jpg]]

** Mehrfachsteckdose in Worten                              :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:

   In Abb. [[fig-mehrfachsteckdose]] sieht man eine Steckdose, an der mehrere Lasten angeschlossen sind. Wir nehmen für dieses Beispiel an, dass alle rein sinusförmigen Wechselstrom der Frequenz @@latex:$f=50\text{ Hz}$@@ verwenden. Dies ist heutzutage unrealistisch, da durch Schaltnetzteile und Frequenzumrichter häufig nichtsinusförmige Stromverläufe mit einem großen Oberwellenanteil (d.h. Überlagerung von Strömen höherer Frequenz) zustandekommen. Jedoch wollen wir zunächst verstehen, was bei der Grundfrequenz geschieht. Daher bleiben wir bei einem reinen sinusförmigen Stromverlauf. Die Ströme der Verbraucher sollen sich lediglich in ihrer Amplitude und in ihrer Phase unterscheiden. 

** Schaltbild

    #+NAME: fig-mehrfachsteckdoseschaltbild
    #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{circuitikz}
       \draw (0,0) to[sinusoidal voltage source] ++(0,-2);
       \draw (0,0) to[short,i>=$i(t)$,-*] ++(2,0) to[short,*-*] ++(2,0) to[short,*-] ++(2,0) to[resistor=$\underline{Z_3}$,i>=$i_3(t)$] ++(0,-2) to[short,-*] ++(-2,0) to[short,*-*] ++(-2,0) to[short,*-] ++(-2,0);
       \draw (2,0) to[resistor=$\underline{Z_1}$,i>=$i_1(t)$,*-*] ++(0,-2);
       \draw (4,0) to[resistor=$\underline{Z_2}$,i>=$i_2(t)$,*-*] ++(0,-2);
     \end{circuitikz}
     \caption{Schaltskizze der Mehrfachsteckdose mit den drei Verbrauchern}\label{fig-mehrfachsteckdoseschaltbild}
   \end{figure}
    #+end_src
** Schaltbild in Worten                                     :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:

   Abb. \ref{fig-mehrfachsteckdoseschaltbild} zeigt eine vereinfachte Schaltungsansicht der Mehrfachsteckdose. Vereinfacht ist hier, dass die Mehrfachsteckdose an einer Spannungsquelle angeschlossen ist (dem Stromnetz), jedoch selbst keine beinhaltet. Es geht uns hier aber um die drei Verbraucher, die mit dem Symbolzeichen @@latex:$\underline{Z}$@@ für den komplexen Widerstand bezeichnet sind. 

   Komplexe Widerstände zeichnen sich dadurch aus, dass sie eine Phasenverschiebung zwischen Strom und Spannung verursachen. Mehr dazu im nächsten Kapitel. 

** Ströme: @@latex:$i(t)=i_1(t)+i_2(t)+i_3(t)$@@

    #+name: fig-dreistroememehrfachsteckdose
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')

plt.rcParams.update({'font.size':fontsize})
plt.rcParams.update({'figure.autolayout': True})

t=np.linspace(0,60e-3,1000)
f = 50
phi1 = np.pi/2
phi2 = np.pi/3
phi3 = -np.pi/4
i1 = 1*np.cos((2*np.pi*f*t)+phi1)
i2 = 0.5*np.cos((2*np.pi*f*t)+phi2)
i3 = 0.8*np.cos((2*np.pi*f*t)+phi3)
iges = i1+i2+i3

fig, ax = plt.subplots(4, 1, sharex=True, sharey=True, figsize=(8, 5))
ax[0].plot(1000*t,i1,'b-')
ax[0].set_ylabel(r'$i_1(t)$ [A]',color='blue')
ax[0].locator_params(nbins=3)
ax[0].grid() # Gitternetzlinien
ax[1].plot(1000*t,i2,'g-')
ax[1].set_ylabel(r'$i_2(t)$ [A]',color='green')
ax[1].grid() # Gitternetzlinien
ax[2].plot(1000*t,i3,'r-')
ax[2].set_ylabel(r'$i_3(t)$ [A]',color='red') # y-Achsentitel
ax[2].grid() # Gitternetzlinien
ax[3].plot(1000*t,iges,'k-')
ax[3].set_ylabel(r'$i(t)$ [A]',color='black')
ax[3].grid()
ax[3].set_xlabel(r'Zeit $t$ [ms]') # x-Achsentitel nur unten

plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Drei Ströme in Mehrfachsteckdose und deren Summe
#+LABEL: fig-dreistroememehrfachsteckdose
#+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-dreistroememehrfachsteckdose
[[file:/tmp/babel-CAKyXZ/figureEj1tlk.png]]


** Ströme addieren im Zeitbereich in Worten                 :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:

  Abb. [[fig-dreistroememehrfachsteckdose]] zeigt die drei Ströme der drei Verbraucher im Zeitbereich. Diese kann man einfach addieren. Dafür legt man für jede n Strom eine Wertetabelle an und addiert zu jedem Zeitpunkt dann die Stromwerte. Da dieses Diagramm im PC mit python entstand, ist es kein Problem eine große Anzahl von Werten zu addieren. 

  Mit der Hand ist das aufwendiger und unpraktischer. 

  Und wenn man nur Variablen hat, müssen die Schwingungen mit den Additionstheoremen für Sinus- und Cosinusfunktionen addiert werden, was leicht mehrere Zeilen an Rechnung mit entsprechenden Fehlermöglichkeiten nach sich zieht.

  Außerdem sind diese Zeitsignale nicht so leicht ablesbar. Beispielsweise dauert es eine Weile, bis man den Nullphasenwinkel sauber abgelesen hat: Erst muss die Periode bestimmt werden, dann die Verschiebung des Signals gegenüber einem Signal mit Nullphasenwinkel Null und dann daraus die Phase berechnet (siehe letztes Kapitel). 

  Da sind komplexe Zahlen trotz ihres Namens einfacher. 

** Kontrollfrage

   Wie kann man herausfinden, ob @@latex:$i(t)$@@ korrekt berechnet wurde?

   #+beamer:\pause

   erste Möglichkeit: zu mehreren Zeitpunkten Werte der Einzelströme ablesen und zusammenzählen: wenn die Summe stimmt, müsste auch die Rechnung korrekt sein

   #+beamer:\pause

   zweite Möglichkeit: komplexe Zahlen verwenden

   - Effektivwert jedes Stroms berechnen und Gesamteffektivwert bestimmen
   - Nullphasenwinkel jedes Stroms bestimmen
   - komplexe Ströme in Polardarstellung skizzieren
   - grafisch als Zeiger addieren

* Komplexe Zahlen: Definition
** Zum Schmunzeln

    #+NAME: fig-complexnumberscool
    #+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
    #+CAPTION: @@latex: \textcolor{gray}{Bild: \href{https://xkcd.com/2028/}{XKCD}, \href{https://creativecommons.org/licenses/by-nc/2.5/legalcode}{CC BY--NC 2.5}}@@ \normalsize
    [[/home/franzi/Bilder/xkcd/complex_numbers.png]]

** Animation und interaktives jupyter notebook

   interaktives jupyter notebook: https://github.com/johannamay/GE2/blob/master/04GE2_komplexezahlen.ipynb

   interaktive Darstellung des Zusammenhangs zwischen Zeigern und Sinusfunktion bzw. Cosinusfunktion: https://www.geogebra.org/m/m7RHfryq#material/FJtrEDAr

   im Video 04b zeige ich beides

** Zeigerdarstellung komplex: Exponentialform

*** Formeldarstellung :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:
   @@latex:$a\angle\varphi_a$@@ wird zu 

   @@latex:\begin{tcolorbox}\begin{equation}
   \underline{a}=a \cdot e^{j\varphi_a} \label{eq-defexponentialformkomplex}
   \end{equation}\end{tcolorbox}@@

   komplexe Zahlen werden _unterstrichen_

   #+BEAMER:\pause
*** Zeigerdiagramm :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:
    
    #+NAME: fig-zeigerdiagrammexponentialform
    #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{tikzpicture}
       \draw[->,color=THKoelnOrange] (-2,0) node[name=a] -- ++(4,0) node[name=b,label=$\Re$];
       \draw[->,color=THKoelnPurple] (0,-2) node[name=c] -- ++(0,4) node[name=d,label=$\Im$];
       \draw[->,thick] (0,0) node[name=e] --++(2,1) node[name=f,label=above:$a$,midway,auto];
       \draw[->,thick] (1,0) arc(0:30:1) node[label=right:$\varphi_a$,auto];
     \end{tikzpicture}
     \caption{Zeiger mit Betrag $a$ und Phase $\varphi_a$}\label{fig-zeigerdiagrammexponentialform}
   \end{figure}
    #+end_src
    
** Zeiger der Ströme im Anwendungsbeispiel: @@latex:$\underline{I}=\underline{I_1}+\underline{I_2}+\underline{I_3}$@@

    #+name: fig-dreistroememehrfachsteckdosezeiger
#+begin_src python :results file :session :var matplot_lib_filename=(org-babel-temp-file "figure" ".png"),fontsize=fs :exports results
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('classic')

plt.rcParams.update({'font.size':fontsize})
plt.rcParams.update({'figure.autolayout': True})

t=np.linspace(0,45e-3,1000)
f = 50
phi1 = np.pi/2
phi2 = np.pi/3
phi3 = -np.pi/4
i1dach = 1
i2dach = .5
i3dach = .8
I1 = i1dach/np.sqrt(2)
I2 = i2dach/np.sqrt(2)
I3 = i3dach/np.sqrt(2)
IW1 = I1*np.cos(phi1)
IW2 = I2*np.cos(phi2)
IW3 = I3*np.cos(phi3)
IB1 = I1*np.sin(phi1)
IB2 = I2*np.sin(phi2)
IB3 = I3*np.sin(phi3)
IW = IW1+IW2+IW3
IB = IB1+IB2+IB3
Iges = np.sqrt(IW**2 + IB**2)
idach = Iges*np.sqrt(2)
theta = np.linspace(0,2*np.pi,1000)
ih1x = I1*np.cos(theta)
ih1y = I1*np.sin(theta)
ih2x = I2*np.cos(theta)
ih2y = I2*np.sin(theta)
ih3x = I3*np.cos(theta)
ih3y = I3*np.sin(theta)
ihx = Iges*np.cos(theta)
ihy = Iges*np.sin(theta)

fig, ax = plt.subplots(1, 4, sharex=True, sharey=True, figsize=(8, 4))
ax[0].set_aspect('equal', adjustable='box')
ax[0].axis([-1.3,1.3,-1.3,1.3])
ax[0].locator_params(nbins=4)
ax[0].plot(ih1x,ih1y,'k:')
ax[0].set_xlabel(r'$I_{1,W}$ [A]', color='blue')
ax[0].set_ylabel(r'$I_{B(i)}$ [A]', color='black')
ax[0].annotate('', xytext = (0,0), xy = (IW1, IB1), 
               arrowprops = {'facecolor' : 'blue',
                             'edgecolor' : 'blue',
                             'linewidth' : .001})
ax[0].grid()
ax[1].set_aspect('equal', adjustable='box')
ax[1].plot(ih2x,ih2y,'k:')
ax[1].set_xlabel(r'$I_{2,W}$ [A]', color='green')
ax[1].annotate('', xytext = (0,0), xy = (IW2, IB2), 
               arrowprops = {'facecolor' : 'green',
                             'edgecolor' : 'green',
                             'linewidth' : .001})
ax[1].grid()
ax[2].set_aspect('equal', adjustable='box')
ax[2].plot(ih3x,ih3y,'k:')
ax[2].set_xlabel(r'$I_{3,W}$ [A]', color='red')
ax[2].annotate('', xytext = (0,0), xy = (IW3, IB3), 
               arrowprops = {'facecolor' : 'red',
                             'edgecolor' : 'red',
                             'linewidth' : .001})
ax[2].grid()
ax[3].set_aspect('equal', adjustable='box')
ax[3].plot(ihx,ihy,'k:')
ax[3].set_xlabel(r'$I_{W}$ [A]', color='black')
ax[3].annotate('', xytext = (0,0), xy = (IW1, IB1), 
               arrowprops = {'facecolor' : 'blue',
                             'edgecolor' : 'blue',
                             'linewidth' : .001})
ax[3].annotate('', xytext = (IW1,IB1), xy = (IW1+IW2, IB1+IB2), 
               arrowprops = {'facecolor' : 'green',
                             'edgecolor' : 'green',
                             'linewidth' : .001})
ax[3].annotate('', xytext = (IW1+IW2,IB1+IB2), xy = (IW, IB), 
               arrowprops = {'facecolor' : 'red',
                             'edgecolor' : 'red',
                             'linewidth' : .001})
ax[3].annotate('', xytext = (0,0), xy = (IW, IB), 
               arrowprops = {'facecolor' : 'black',
                             'edgecolor' : 'black',
                             'linewidth' : .001})
ax[3].grid()

plt.tight_layout()
plt.savefig(matplot_lib_filename)
matplot_lib_filename
#+end_src

#+CAPTION: Drei Ströme in Mehrfachsteckdose und deren Summe im Zeigerdiagramm
#+LABEL: fig-dreistroememehrfachsteckdosezeiger
#+ATTR_LATEX: :width \textwidth :height \textheight :options angle=0,keepaspectratio :float nil
#+RESULTS: fig-dreistroememehrfachsteckdosezeiger



   
** Komplexe Einheit @@latex:$j=\sqrt{-1}$@@

    #+NAME: fig-komplexeeinheitzeiger
    #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{tikzpicture}
       \draw[->,thick,color=THKoelnPurple] (0,0) -- ++(0,2) node[name=g,label=above:$\color{black}{e^{j90^\circ}=j^1=j}$,auto];
       \pause
       \draw[->,thick,color=THKoelnOrange] (0,0) -- ++(-2,0) node[name=h,label=above:$\color{black}{e^{j180^\circ}=j^2=-1}$, auto];
       \pause
       \draw[->,thick,color=THKoelnPurple] (0,0) -- ++(0,-2) node[name=i,label=right:$\color{black}{e^{j270^\circ}=e^{-j90^\circ}=\frac{1}{j}=j^3=-j}$, auto];
       \pause
       \draw[->,thick,color=THKoelnOrange] (0,0) node[name=e] --++(2,0) node[name=f,label=above:$\color{black}{e^{j\cdot0^\circ}=\frac{j}{j}=j^4=1}$,auto];
     \end{tikzpicture}
     \caption{Komplexe Einheit $j$ in der komplexen Ebene}\label{fig-komplexeeinheitzeiger}
   \end{figure}
    #+end_src

** Zeigerdarstellung komplex: Normalform

*** Formeln :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:
   @@latex:$\underline{a}=a \cdot e^{j\varphi_a}$@@ kann man auch schreiben als

   @@latex:\begin{tcolorbox}\begin{equation}
   \underline{a}=\color{THKoelnOrange}{\Re(\underline{a})} + j \color{THKoelnPurple}{\Im (\underline{a})} = \color{THKoelnOrange}{a_W} + j \color{THKoelnPurple}{a_B}
   \end{equation}\end{tcolorbox}@@

   mit den Indizes 
   - @@latex:$\color{THKoelnOrange}{W}$@@ für den @@latex:\textcolor{THKoelnOrange}{Realteil}@@, in der Elektrotechnik @@latex:\textcolor{THKoelnOrange}{\textbf{Wirkanteil}}@@
   - @@latex:$\color{THKoelnPurple}{B}$@@ für den @@latex:\textcolor{THKoelnPurple}{Imaginärteil}@@, in der Elektrotechnik @@latex:\textcolor{THKoelnPurple}{\textbf{Blindanteil}}@@

\normalsize
#+BEAMER:\pause
*** Zeigerdiagramm :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:
    #+NAME: fig-zeigerdiagrammnormalform
    #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{tikzpicture}
       \draw[->] (-1,0) node[name=a] -- ++(5,0) node[name=b,label=$\Re$];
       \draw[->] (0,-1) node[name=c] -- ++(0,5) node[name=d,label=$\Im$];
       \draw[->,thick] (0,0) node[name=e] --++(1.5,3) node[name=f,label={rotate=60,left:$\color{black}{a=\sqrt{\color{THKoelnOrange}{a_W}^2 + \color{THKoelnPurple}{a_B}^2}}$},midway,auto];
       \pause
       \draw[->,thick] (1,0) arc(0:60:1) node[label=right:$\color{black}{\varphi_a=\arctan(\frac{\color{THKoelnPurple}{a_B}}{\color{THKoelnOrange}{a_W}})}$, auto];
       \pause
       \draw[->,thick,color=THKoelnOrange] (0,0) --++(1.5,0) node[name=g,label=below:${\color{THKoelnOrange}{a_W}\color{THKoelnOrange}{=a\cdot \cos\varphi_a}}$,auto];
       \pause
       \draw[->,thick,THKoelnPurple] (1.5,0) --++(0,3) node[name=h,label=right:${\color{THKoelnPurple}{a_B}\color{THKoelnPurple}{=a\cdot\sin\varphi_a}}$,auto];
     \end{tikzpicture}
     \caption{Zeiger mit Betrag $a$ und Phase $\varphi_a$}\label{fig-zeigerdiagrammnormalform}
   \end{figure}
    #+end_src

** Zeigerdarstellung in Worten :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:

   /Hinweis: Die komplexe Einheit heißt in der Elektrotechnik @@latex:$j$@@ und nicht wie in der Mathematik (und auf Ihrem Taschenrechner) @@latex:$i$@@. Das hat den Hintergrund, dass man sonst leicht die komplexe Einheit mit dem Strom verwechseln könnte. Eine weitere Verwechslungsgefahr besteht dann zwar noch mit der Stromdichte @@latex:$\vec{j}$@@. Da man allerdings in der komplexen Wechselstromrechnung fast nie mit der Stromdichte rechnet, ist auch hier die Verwechslungsgefahr gering./

   Schreibt man Effektivwert und Phase als @@latex:$a\angle\varphi_a$@@ für ein Signal, kann man dieses in der komplexen Darstellung als eine _unterstrichene_ Größe mit @@latex:$\underline{a}$@@ in Gleichung \ref{eq-defexponentialformkomplex} nennen. Sie sehen, dass nun als Betrag weiterhin eine Zahl steht, die der Länge des Zeigers entspricht. Hierfür nimmt man in der Elektrotechnik den Effektivwert des Signals, weil dieser Wert sehr wichtig ist, um viele Dinge einzuschätzen. Außerdem sieht man den Phasenwinkel, kurz die *Phase* (/phase/) des Signals, @@latex:$\varphi_a$@@ im Exponenten. Weiterhin können Sie im Taschenrechner so eine Zahl eingeben mit @@latex:$a\angle\varphi_a$@@. Die graphische Darstellung so eines Zeigers ist in Abb. \ref{fig-zeigerdiagrammexponentialform} zu finden. Die Phase wird also immer positiv gezählt, wenn man im Uhrzeigersinn läuft und negativ gegen den Uhrzeigersinn. 

   Die komplexe Einheit @@latex:$j$@@ ist von der mathematischen Definition her einfach die Quadratwurzel aus @@latex:$-1$@@. Man kann sie auch in der komplexen Ebene darstellen, wie Abb. \ref{fig-komplexeeinheitzeiger} zeigt. Darüber kann man sich vergegenwärtigen, dass die komplexe Einheit sich auch in der Exponentialform schreiben lässt (Gleichung [[eq-komplexeeinheitexponentialform]]):

   #+name:eq-komplexeeinheitexponentialform
   \begin{equation}
   j=e^{j90^\circ}
   \end{equation}

   Aus der Zeigerdarstellung mit der Exponentialform \ref{fig-zeigerdiagrammexponentialform} sieht man auch, dass es auch möglich ist, wie bei Vektoren, eine Darstellung zu finden, bei der der x-Anteil und der y-Anteil verwendet wird. Der x-Anteil heißt in der Elektrotechnik @@latex:\textcolor{THKoelnOrange}{\textbf{Wirkanteil}}@@. Der y-Anteil heißt in der Elektrotechnik @@latex:\textcolor{THKoelnPurple}{\textbf{Blindanteil}}@@. In der Mathematik ist der @@latex:\textcolor{THKoelnOrange}{Wirkanteil}@@ allgemeiner der @@latex:\textcolor{THKoelnOrange}{\textbf{Realteil}}@@ (@@latex:\textcolor{THKoelnOrange}{\textit{real part}}@@) einer komplexen Zahl, der @@latex:\textcolor{THKoelnPurple}{Blindanteil}@@ allgemeiner der @@latex:\textcolor{THKoelnPurple}{\textbf{Imaginärteil}}@@ (@@latex:\textcolor{THKoelnPurple}{\textit{imaginary part}}@@) einer komplexen Zahl. Aus den Winkelbeziehungen kann man eine ganze Reihe weiterer Formelzusammenhänge im Zeigerdiagramm ableiten, die Abb. \ref{fig-zeigerdiagrammnormalform} zusammenfasst. So kann man aus @@latex:\textcolor{THKoelnOrange}{Wirk-}@@ und @@latex:\textcolor{THKoelnPurple}{Blindanteil}@@ die Phase und den Betrag ausrechnen und umgekehrt. 

* Komplexe Zahlen: Rechenregeln
** Rechnen mit Zeigern: Addition und Subtraktion

*** Spannungen in unserem Anwendungsbeispiel :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:

    #+NAME: fig-esbmotorbspzeigerkomplex
    #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{circuitikz}[scale=.8]
       \draw (0,0) node[name=a] to[sinusoidal voltage source=$\color{OwlGreen}{230\text{ V}\cdot e^{j\varphi_q}}$] ++(0,-5) node[name=b];
       \draw (a) to[resistor=$\color{black}{R_{\text{Leitung}}=\rho\cdot\frac{l}{A_N}}$,v=$\color{OwlGreen}{U_{RL1} \cdot e^{j\varphi_{RL1}}}$] ++(4,0) node[name=c];
       \draw (b) to[resistor=$\color{black}{R_{\text{Leitung}}=\rho\cdot\frac{l}{A_N}}$,v=$\color{OwlGreen}{U_{RL2}\cdot e^{j\varphi_{RL2}}}$] ++(4,0) node[name=d];
       \draw (c) to[resistor=$\color{black}{R_{\text{Motor}}}$,v=$\color{OwlGreen}{U_{MR}\cdot e^{j\varphi_{MR}}}$] ++(0,-2.5) node[name=e];
       \draw (e) to[american inductor=$\color{black}{L_{\text{Motor}}}$,v=$\color{OwlGreen}{U_{ML}\cdot e^{j\varphi_{ML}}}$] (d);
     \end{circuitikz}
     \caption{Komplexe Spannungen an einem Wechselstrommotor im Nennbetrieb, angeschlossen über Anschlusskabel an das elektrische Stromnetz, Werte für den Motor: MSF-Vathauer Antriebstechnik AM 90/4 Einphasen-Käfigläufermotor, gesehen bei \href{https://www.conrad.de/de/p/msf-vathauer-antriebstechnik-am-90-4-einphasen-kaefiglaeufermotor-128850.html}{conrad}}\label{fig-esbmotorbspzeigerkomplex}
   \end{figure}
    #+end_src

*** Grafische Ermittlung der Spannung @@latex:$\underline{U_{LM}}$@@  :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .5
    :END:

    #+NAME: fig-esbmotorbspzeigerkomplexadditionsdiagramm
    #+begin_src latex :results raw
   \begin{figure}[h]
     \centering
     \begin{tikzpicture}
       \draw[->,color=THKoelnOrange] (-2,0) node[name=a] -- ++ (4,0) node[name=b,label=$\Re{}$];
       \draw[->,color=THKoelnPurple] (0,-2) node[name=c] -- ++ (0,4) node[name=d,label=$\Im{}$];
       \pause
       \draw[->,thick] (0,0) node[name=e] -- ++(1.5,0) node[name=f,label=$\underline{U_q}$];
       \pause
       \draw[->,thick] (1.5,0) -- ++(-.5,-.5) node[name=g,label=right:$-\underline{U_{RL1}}$];
       \pause
       \draw[->,thick] (1,-.5) --++(-.5,-.5) node[name=h,label=right:$-\underline{U_{RL2}}$];
       \pause
       \draw[->,thick] (.5,-1) --++(-1,-1) node[name=i,label=right:$-\underline{U_{RM}}$];
       \pause
       \draw[->,thick,THKoelnRed] (0,0) --++(-.5,-2) node[name=j,label=left:$\underline{U_{LM}}$];
     \end{tikzpicture}
     \caption{Maschenregel grafisch aufgelöst mit Beispielwerten}\label{fig-esbmotorbspzeigerkomplexadditionsdiagramm}
   \end{figure}
    #+end_src

    Addition und Subtraktion mit Zeigern: wie mit Vektoren (Abb. \ref{fig-esbmotorbspzeigerkomplexadditionsdiagramm})
 
** Addition und Subtraktion in Worten                       :B_ignoreheading:
   :PROPERTIES:
   :BEAMER_env: ignoreheading
   :END:

   Abb. \ref{fig-esbmotorbspzeigerkomplex} zeigt eine Schaltung, in dem das elektrische Verhalten eines elektrischen Motors mit einer Induktivität @@latex:$L_{\text{Motor}}$@@ (die Wicklungen, die das Magnetfeld erzeugen) und einem Ohmschen Widerstand (des Drahtes in den Wicklungen) @@latex:$R_{\text{Motor}}$@@ beschrieben wird. Hinzu kommen Leitungswiderstände @@latex:$R_{\text{Leitung}}$@@ entsprechend dem spezifischen Widerstand des Leitungsmaterials @@latex:$\rho$@@, dem Querschnitt @@latex:$A_N$@@ der Leitung und der Länge @@latex:$l$@@ der Leitung. 

   Die verschiedenen Spannungen lassen sich mit komplexen Zeigern beschreibung und hängen über die Maschenregel miteinander zusammen. So kann man die im Beispiel unbekannte Spannung an der Induktivität des Motors @@latex:$\underline{U_{LM}$@@ bestimmen und die entsprechenden Zeiger grafisch oder mathematisch miteinander verrechnen.

** Rechnen mit Zeigern: Division

   Zeiger @@latex:$\underline{a}$@@ durch Zeiger @@latex:$\underline{b}$@@ dividieren:
   #+BEAMER:\pause

   in Exponentialform: Phasenwinkel subtrahieren
   #+BEAMER:\pause
   #+name:eq-zeigerdivexponential
   \begin{equation}
   \frac{\underline{a}}{\underline{b}}=\frac{a\cdot e^{j\varphi_a}}{b\cdot e^{j\varphi_b}}=\frac{a}{b}\cdot e^{j(\varphi_a - \varphi_b)}
   \end{equation}
   #+BEAMER:\pause

   in Normalform: Nenner reell machen
   #+BEAMER:\pause
   #+name:eq-zeigerdivnormal
   \begin{equation}
   \frac{\underline{a}}{\underline{b}}=\frac{\color{THKoelnOrange}{a_W} + j\color{THKoelnPurple}{a_B}}{\color{THKoelnOrange}{b_W} + j\color{THKoelnPurple}{b_B}}=\frac{(\color{THKoelnOrange}{a_W} + j\color{THKoelnPurple}{ a_B})}{(\color{THKoelnOrange}{b_W} + j\color{THKoelnPurple}{b_B})}\cdot \frac{(\color{THKoelnOrange}{b_W} - j\color{THKoelnPurple}{b_B})}{(\color{THKoelnOrange}{b_W} -j\color{THKoelnPurple}{b_B})}=\frac{(\color{THKoelnOrange}{a_W} + j\color{THKoelnPurple}{a_B})\cdot(\color{THKoelnOrange}{b_W} - j\color{THKoelnPurple}{b_B})}{\color{THKoelnOrange}{b_W}^2 + \color{THKoelnPurple}{b_B}^2}
   \end{equation}

** Rechnen mit Zeigern: Multiplikation

   Zeiger @@latex:$\underline{a}$@@ mit Zeiger @@latex:$\underline{b}$@@ multiplizieren:
   #+BEAMER:\pause

   in Exponentialform: Phasenwinkel addieren
   #+BEAMER:\pause
   #+name:eq-zeigerdivexponential
   \begin{equation}
   \underline{a}\cdot\underline{b}=a\cdot e^{j\varphi_a} \cdot b\cdot e^{j\varphi_b}=ab \cdot e^{j(\varphi_a + \varphi_b)}
   \end{equation}
   #+BEAMER:\pause

   in Normalform: binomische Formeln anwenden
   #+BEAMER:\pause
   #+name:eq-zeigerdivnormal
   \begin{equation}
   \underline{a}\cdot\underline{b}=(\color{THKoelnOrange}{a_W} + j\color{THKoelnPurple}{a_B})\cdot(\color{THKoelnOrange}{b_W} + j\color{THKoelnPurple}{b_B}) = \color{THKoelnOrange}{a_W b_W - a_B b_B} + j\color{THKoelnPurple}{(a_W b_B + a_B b_W)}
   \end{equation}

** Konjugiert komplexe Größen

   *konjugiert komplex*:

   #+NAME: tab-konjugiertkomplexe
   #+ATTR_LATEX: :float nil
   #+CAPTION: Konjugiert komplexe Größe
   |--------------------+----------------------------+-------------------------------------------+----------------------|
   | Bezeichnung        | Größe                      | Exponentialform                           | Normalform           |
   |--------------------+----------------------------+-------------------------------------------+----------------------|
   | ursprünglich       | @@latex:$\underline{a}$@@  | @@latex:$a \cdot e^{j\varphi_a}$@@                     | @@latex:$\color{THKoelnOrange}{a_W} + j\color{THKoelnPurple}{a_B}$@@ |
   | konjugiert komplex | @@latex:$\underline{a^{\color{THKoelnRed}{*}}}$@@ | @@latex:$a\cdot e^{\color{THKoelnRed}{-j\varphi_a}}$@@ | @@latex:$\color{THKoelnOrange}{a_W} \color{THKoelnRed}{-} j \color{THKoelnPurple}{a_B}$@@ |

   Beispiel: komplexe Scheinleistung @@latex:$\underline{S}=\underline{U}\cdot\underline{I^*}$@@
** Rechnen mit drehenden Zeigern

   da es um Zeitfunktionen geht, muss man die hier wieder beachten

   man nimmt also nun nicht nur die Momentaufnahme bei @@latex:$t=0$@@ @@latex:$\underline{U}=U \cdot e^{j\varphi}$@@, sondern man setzt auch den Zeitaspekt wieder ein:

   \begin{equation}
   \underline{U} = U \cdot e^{j(\omega t + \varphi)}
   \end{equation}

   dieser Zeiger rotiert mit @@latex:$\omega$@@ (wie in der geogebra-App gezeigt)

   https://www.geogebra.org/m/m7RHfryq#material/FJtrEDAr

** Differenzieren von Zeigern

   das ist nur sinnvoll bei zeitabhängigen (rotierenden) Zeigern, da auch bei komplexen Zahlen die Ableitung eines Ausdrucks, der nicht von @@latex:$t$@@ abhängt, Null ist

   \begin{equation}
   \frac{d}{dt}\underline{U} = \frac{d}{dt}\left(U\cdot e^{j(\omega t+ \varphi)}\right) = j\cdot U\cdot \omega \cdot e^{j(\omega t + \varphi)} = U\cdot \omega \cdot e^{j(\omega t + \varphi + \pi/2)}
   \end{equation}

   Achtung: beim Nachdifferenzieren das @@latex:$j$@@ nicht vergessen

** Integrieren von Zeigern

   \begin{equation}
   \int_t \underline{U} dt = \int_t U \cdot e^{j(\omega t + \varphi)} dt = U \cdot \frac{1}{j\omega}\cdot e^{j(\omega t + \varphi)} = \frac{U}{\omega} \cdot e^{j(\omega t + \varphi - \pi/2)}
   \end{equation}

   eigentlich alles wie bei reellen Funktionen - bis auf das @@latex:$j$@@ 

* Zusammenfassung                                                   :B_frame:
  :PROPERTIES:
  :BEAMER_env: frame
  :END:
\normalsize

*** Begriffe und Formeln :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .7
    :END:
    - Zeitfunktion @@latex:$u(t)=\hat{u}\cdot\cos(\omega t+\varphi)$@@ äquivalent zu Effektivwertzeiger @@latex:$\underline{U}=U\cdot e^{j\varphi} = U_W + jU_B$@@
      #+BEAMER:\pause
    - komplexe Einheit @@latex:$\sqrt{-1}=j=e^{j90^\circ}=e^{j\pi/2}$@@
      #+BEAMER:\pause
    - Addieren / Subtrahieren wie Vektoren
      #+BEAMER:\pause
    - Multiplikation: @@latex:$\underline{A_1}\cdot\underline{A_2}=A_1 \cdot e^{j\varphi_1} \cdot A_2 \cdot e^{j\varphi_2} = A_1 \cdot A_2 \cdot e^{j(\varphi_1 + \varphi_2)}$@@
      #+BEAMER:\pause
    - Division: @@latex:$\frac{\underline{A_1}}{\underline{A_2}}=\frac{A_1}{A_2}\cdot e^{j(\varphi_1 - \varphi_2)}$@@
      #+BEAMER:\pause
*** Hausaufgaben :B_column:
    :PROPERTIES:
    :BEAMER_env: column
    :BEAMER_col: .3
    :END:
    \footnotesize
    - Übungsaufgaben, Tutorium
      #+BEAMER:\pause
    - Praktikum anmelden und vorbereiten
\normalsize
