#+STARTUP: beamer

#+LATEX_COMPILER: lualatex

#+LaTeX_HEADER: \definecolor{THKoelnRed}{RGB}{201,012,015}
#+LaTeX_HEADER: \definecolor{THKoelnOrange}{RGB}{234,091,012}
#+LaTeX_HEADER: \definecolor{THKoelnPurple}{RGB}{180,048,146}

#+LaTeX_CLASS: beamer
#+BEAMER_HEADER: \usepackage{fontspec}
#+LaTeX_CLASS_OPTIONS: [ignorenonframetext,presentation,ngerman,allowframebreaks,t]
#+BEAMER_THEME: 
#+BEAMER_FONT_THEME: metropolis
#+BEAMER_INNER_THEME: metropolis
#+BEAMER_OUTER_THEME: [progressbar=frametitle,numbering=fraction]{metropolis}
#+BEAMER_COLOR_THEME: [snowy,cautious]{owl}
#+LOGO: \includegraphics{/home/franzi/Bilder/TH/thk.pdf}
#+BEAMER_HEADER: \setbeamercolor{frametitle}{fg=THKoelnRed,bg=white}
#+BEAMER_HEADER: \setbeamercolor{framesubtitle}{fg=THKoelnRed,bg=white}
#+BEAMER_HEADER: \setbeamercolor{normal text}{fg=black}
#+BEAMER_HEADER: \setbeamercolor{alerted text}{fg=THKoelnRed}
#+BEAMER_HEADER: \setbeamercolor{example text}{fg=THKoelnPurple}
#+BEAMER_HEADER: \setbeamercolor{progress bar}{fg=THKoelnOrange}
#+BEAMER_HEADER: \setbeamercolor{title separator}{fg=THKoelnOrange}
#+BEAMER_HEADER: \setbeamercolor{progress bar in head/foot}{fg=THKoelnOrange}
#+BEAMER_HEADER: \setbeamercolor{progress bar in section page}{fg=THKoelnOrange}
# #+BEAMER_HEADER: \usepackage{eulervm}
#+BEAMER_HEADER: \setmainfont{FiraSans-Book.otf}[BoldFont=FiraSans-SemiBold.otf,ItalicFont=FiraSans-BookItalic.otf,BoldItalicFont=FiraSans-BoldItalic.otf]
# #+BEAMER_HEADER: \setsansfont{FiraSans-Book.otf}[BoldFont=FiraSans-SemiBold.otf,ItalicFont=FiraSans-BookItalic.otf,BoldItalicFont=FiraSans-BoldItalic.otf]

#+BEAMER_HEADER: \makeatletter
#+BEAMER_HEADER: \setlength{\metropolis@frametitle@padding}{2.2ex}
# #+BEAMER_HEADER: \setbeamertemplate{frametitle}{\nointerlineskip\begin{beamercolorbox}[wd=\paperwidth,sep=0pt,leftskip=\metropolis@frametitle@padding,rightskip=\metropolis@frametitle@padding,]{frametitle}\metropolis@frametitlestrut@start\insertframetitle\nolinebreak\metropolis@frametitlestrut@end\hfill\includegraphics[height=1.5ex,keepaspectratio]{/home/franzi/Bilder/TH/thk.pdf}\end{beamercolorbox}}
#+BEAMER_HEADER: \makeatother

#+LATEX_HEADER: \usepackage{verbatim} % damit es zwischen bibtex und biblatex keinen Konflikt gibt
#+LaTeX_HEADER: \usepackage[utf8]{luainputenc}
#+LaTeX_HEADER: \usepackage[ngerman]{babel}
#+LATEX_HEADER: \usepackage{pgfpages}
#+LATEX_HEADER: \usepackage{pgfplots}
#+LaTeX_HEADER: \usepackage{svg}
#+LaTeX_HEADER: \usepackage{subfig}

# Local Variables:
# org-ref-default-bibliography: /home/franzi/Dokumente/lehren/1module/GE2/orgscript/GE2skript/GE2.bib
# End:
#+LATEX_HEADER: \usepackage[style=alphabetic,backend=biber,bibencoding=utf8]{biblatex}
#+LATEX_HEADER: \addbibresource{/home/franzi/Dokumente/lehren/1module/GE2/orgscript/GE2skript/GE2.bib}
# #+LATEX_HEADER: \usepackage{tabularx,siunitx,booktabs,graphicx,ngerman} %für Tabellen nach http://archive.indianstatistics.org/tools/orgpapers.pdf
#+LaTeX_HEADER: \usepackage{booktabs}
#+LaTeX_HEADER: \usepackage[table]{xcolor} %für farbige Tabellenzellen
#+LaTeX_HEADER: \usepackage{colortbl}
#+LaTeX_HEADER: \usepackage{siunitx}
#+LaTeX_HEADER: \usepackage{amsmath}
#+LaTeX_HEADER: \usepackage{amsfonts}
#+LaTeX_HEADER: \usepackage{amssymb}
# #+LATEX_HEADER: \sisetup{locale=DE,per-mode=symbol}
#+LaTeX_HEADER: \usepackage[european resistor, american inductor,straightvoltages,oldvoltagedirection]{circuitikzgit}
#+LaTeX_HEADER: \usepackage{tikz}
#+LaTeX_HEADER: \usepackage{tkz-euclide}
#+LaTeX_HEADER: \usetkzobj{all}
#+LaTeX_HEADER: \usetikzlibrary{mindmap,trees,quotes,arrows.meta,positioning,shapes.geometric,decorations.pathmorphing,angles}
#+LaTeX_HEADER: \usetikzlibrary{calc,matrix}
#+LaTeX_HEADER: \usetikzlibrary{decorations.markings}
# #+LaTeX_HEADER: \usetikzlibrary{circuits.ee.IEC}

#+LaTeX_HEADER: \usepackage[type={CC},modifier={by-sa},version={4.0},]{doclicense}

